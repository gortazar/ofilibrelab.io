---
layout: archive
permalink: /creditos.html
title: "Créditos"
image:
  feature: logo-ofilibre-blanco.jpg
---

## Alojamiento

Este sitio está alojado en [GitLab pages](https://docs.gitlab.com/ee/user/project/pages/).

## Software

Este sitio web está construido con [Jekyll](http://jekyllrb.com), usando el estilo [Skinny Bones](https://mmistakes.github.io/jekyll-theme-skinny-bones/).

## Imágenes

En varias partes de este sitio se utilizan imágenes de terceros con licencias libres:

<img src="/images/catalogo/logo.png" height="71" width="80">
[learn-online-book-silhouette-icon-53](https://pixabay.com/vectors/learn-online-book-silhouette-icon-5377778/), por [mohamed_hassan](https://pixabay.com/users/mohamed_hassan-5229782/)
