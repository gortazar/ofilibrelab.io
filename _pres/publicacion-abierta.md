---
title: Publicación Abierta
date: 2019-11-25
image:
  feature: Publicacion_Abierta.png
  teaser: Publicacion_Abierta.png
transpas:
  - format: PDF
    file: Publicacion_Abierta.pdf
  - format: ODP (LibreOffice)
    file: Publicacion_Abierta.odp
extras:
  - name: Presentación utilizada en el taller sobre publicación abierta que tuvo lugar en los Talleres de Innovación Educativa y Cultura Abierta, URJC campus de Alcorcón, 25 de noviembre de 2019.
    url: ../transpas/publicacion-abierta/Publicacion_Abierta-jjii-2019.pdf
  - name: Ficha del taller sobre publicación abierta que tuvo lugar en los Talleres de Innovación Educativa y Cultura Abierta, URJC campus de Alcorcón, 25 de noviembre de 2019.
    url: ../transpas/publicacion-abierta/publicacion-abierta-jornadas-innovacion-2019.pdf
---

Presentación sobre publicación abierta, desde un punto de vista práctico.

Versión presentada en la sesión "Publicación abierta" del Seminario sobre ética de la investigación, organizado por la Esuela Internacional de Doctorado de la URJC, 28 de mayo de 2020.

* [Videos de la presentación](https://tv.urjc.es/series/5ee201f2d68b14531a8b464d)