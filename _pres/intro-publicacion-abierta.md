---
title: Introduccion a la Publicación Abierta
date: 2020-03-09
image:
  feature: Intro_Publicacion_Abierta.png
  teaser: Intro_Publicacion_Abierta.png
transpas:
  - format: PDF
    file: Intro_Publicacion_Abierta.pdf
  - format: ODP (LibreOffice)
    file: Intro_Publicacion_Abierta.odp
extras:
  - name: Presentación en el campus de Fuenlabrada
    url: ../blog/evento-pub-abierta-fuenla-09-03/
---

Introducción a la publicación abierta, enfocada tanto a la publicación de resultados de investigación, como de matariales docentes.

